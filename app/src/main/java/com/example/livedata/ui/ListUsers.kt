package com.example.livedata.ui

import com.google.gson.annotations.SerializedName


data class ListUsers(
    var page: Int? = 2,
    @SerializedName("per_page") var perPage: Int? = 6,
    var total: Int? = 2,
    @SerializedName("total_pages") var totalPages: Int? = 2,
    var data: ArrayList<Users>? = ArrayList()
) {
    data class Users(
        var id: Int? = 0,
        var email: String? = "",
        @SerializedName("first_name") var firstName: String? = "",
        @SerializedName("last_name") var lastName: String? = "",
        @SerializedName("avatar") var url: String? = ""
    )
}