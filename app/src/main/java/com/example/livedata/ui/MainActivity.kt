package com.example.livedata.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.livedata.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val usersViewModel: UsersViewModel by viewModel(
        UsersViewModel::class)
    private lateinit var usersRecyclerViewAdapter: UsersRecyclerViewAdapter
    private var listUsers = ListUsers()
    private var users = mutableListOf<ListUsers.Users>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        initRecyclerView()
    }

    private fun init() {
        usersViewModel.users.observe(this, Observer {
            listUsers = it
            d("listeners", listUsers.toString())
            users.addAll(listUsers.data!!)
            usersRecyclerViewAdapter.notifyDataSetChanged()
        })
    }

    private fun initRecyclerView() {
        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        usersRecyclerViewAdapter =
            UsersRecyclerViewAdapter(users)
        usersRecyclerView.adapter = usersRecyclerViewAdapter
        usersRecyclerViewAdapter.notifyDataSetChanged()
    }

}