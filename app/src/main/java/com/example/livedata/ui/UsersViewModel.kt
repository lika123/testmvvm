package com.example.livedata.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.livedata.koin.network.UsersCallback
import com.example.livedata.koin.network.Endpoint
import com.example.livedata.koin.network.Repository

class UsersViewModel(private val repository: Repository) : ViewModel() {

    init {
        loadUsers()
    }

    val users: MutableLiveData<ListUsers> = MutableLiveData()

    private fun loadUsers() = repository.getUsers(Endpoint.USERS, object :
        UsersCallback {
        override fun onError(error: String, body: String) {

        }

        override fun onSuccess(response: String, body: ListUsers) {
            users.value = body
        }
    })
}