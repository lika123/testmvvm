package com.example.livedata.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.livedata.R
import kotlinx.android.synthetic.main.user_item.view.*

class UsersRecyclerViewAdapter(private val users: MutableList<ListUsers.Users>) :
    RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: ListUsers.Users
        fun onBind() {
            model = users[adapterPosition]
            itemView.idTV.text = model.id.toString()
            itemView.emailTV.text = model.email.toString()
            itemView.firstNameTV.text = model.firstName.toString()
            itemView.lastNameTV.text = model.lastName.toString()
            Glide
                .with(itemView)
                .load(model.url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(itemView.avatarIV)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false))

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}