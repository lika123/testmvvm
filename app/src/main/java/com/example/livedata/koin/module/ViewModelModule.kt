package com.example.livedata.koin.module

import com.example.livedata.koin.network.Repository
import com.example.livedata.ui.UsersViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    single { Repository() }
    viewModel { UsersViewModel(get()) }

}