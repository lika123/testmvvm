package com.example.livedata.koin.network

import com.example.livedata.ui.ListUsers


interface UsersCallback {
    fun onError(error: String, body: String)
    fun onSuccess(response: String, body: ListUsers)
}