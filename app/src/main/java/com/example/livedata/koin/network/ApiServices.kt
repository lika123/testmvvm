package com.example.livedata.koin.network

import com.example.livedata.ui.ListUsers
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiServices {
    @GET("{path}")
    fun getUsers(@Path("path") user: String?): Call<ListUsers>
}