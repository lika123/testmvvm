package com.example.livedata.koin.network

import com.example.livedata.ui.ListUsers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository {

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://reqres.in/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    var service: ApiServices = retrofit.create(
        ApiServices::class.java)

    fun getUsers(path: String, usersCallback: UsersCallback) {
        val call = service.getUsers(path)
        call.enqueue(onCallBack(usersCallback))
    }

    private fun onCallBack(usersCallback: UsersCallback) = object : Callback<ListUsers> {
        override fun onFailure(call: Call<ListUsers>, t: Throwable) {
            usersCallback.onError("error", t.message.toString())

        }

        override fun onResponse(call: Call<ListUsers>, response: Response<ListUsers>) {
            usersCallback.onSuccess("response", response.body()!!)

        }
    }
}